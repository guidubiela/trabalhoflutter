import 'package:flutter/material.dart';
import 'package:projeto/compraRealizada.dart';
import 'package:projeto/compras.dart';
import 'produtos.dart';
import 'sobre.dart';

void main() {
  runApp(Padaria());
}

class Padaria extends StatelessWidget {
  final tema = ThemeData(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.amber,
      titleTextStyle: TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.bold,
        color: Colors.black
      )
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        backgroundColor: Colors.amber,
      )
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.amber,
      foregroundColor: Colors.black
    )
  );


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Padaria App',
      theme: tema,
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        '/sobre': (context) => SobreScreen(),
        '/produtos':(context) => ProductListScreen(),
        '/compraRealizada': (context) => PurchaseScreen(),
        '/compras':(context) => Compras()
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 247, 217, 128),
      appBar: AppBar(
        title: Text('Tela Inicial'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/padoca.png',
              width: 200,
              height: 300,
            ),
            SizedBox(height: 20),
            Text(
              'Bem-vindo à Padoca do Seu Zé',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/produtos');
              },
              child: Text(
                'Começar',
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/sobre');
              },
              child: Text(
                'Sobre',
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/compras');
              },
              child: Text(
                'Compras',
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

